const { DataStore } = require('notarealdb');
const mysql = require('mysql2/promise');
const store = new DataStore('./data');

let mysql_data = {
	students: [],
	colleges: []
};

module.exports = (async function(){
	return {
		students:store.collection('students'),
		studentsag:store.collection('students'),
		colleges:store.collection('colleges'),
		collegesmy:mysql_data.colleges,
		studentsmy:mysql_data.students		
	}	
})();