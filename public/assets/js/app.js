window.app = (function() {
    let root = {};

    const _init = function() {
        _jquery();
        _vue();
        _agGrid();
    };

    const _jquery = function() {
        $('.btnReset').on('click', function() {
            let init_tab = "welcome";
            let full_clean = 0;
            if ($("#greetingDiv").html() === "" 
                && $("#SayhelloDiv").html() === "" 
                && app.students.length === 0 
                && app.studentsag.length === 0
            ) {
                $('#tab_example a[href="#' + init_tab + '"]').tab('show')
                $(".errorModal .modal-body p").html("No hay nada que limpiar.");
                $('.errorModal').modal('show');
                return;
            }

            if (app.studentsag.length !== 0) {
                app.studentsag = [];
                app.ag.gridOptions.api.setRowData(app.studentsag);
                full_clean++;
            }
            if (app.students.length !== 0) {
                app.students = [];
                app.vue_students.students = app.students;
                full_clean++;
            }
            if ($("#SayhelloDiv").html() !== "") {
                $("#SayhelloDiv").html('');
                full_clean++;
            }
            if ($("#greetingDiv").html() !== "") {
                $("#greetingDiv").html('');
                full_clean++;
            }

            if (full_clean == 4) {
                $('a[href="#' + init_tab + '"]').trigger('click');
            }
        });

        $("#btnGreet").off('click').on('click', function() {
            $(".spinner").removeClass("d-none");
            let query = JSON.stringify({
                query: '{greeting}'
            });
            $.ajax({
                url: "http://localhost:9000/graphql",
                contentType: "application/json",
                type: 'POST',
                data: query,
                success: function(result) {
                    $("#greetingDiv").html("<h3>" + result.data.greeting + "</h3>");
                    $(".spinner").addClass("d-none");
                },
                error(xhr, options, msg) {
                    var msg = typeof xhr.responseJSON != "undefined" ? xhr.responseJSON.errors[0].message : xhr.statusText;
                    $(".errorModal .modal-body p").html(msg);
                    $('.errorModal').modal('show');
                    $(".spinner").addClass("d-none");
                }
            });
        });

        $("#btnSayhello").off('click').on('click', function() {
            $(".spinner").removeClass("d-none");
            let name = $("#txtName").val();
            let query = JSON.stringify({
                query: '{sayHello(name:"' + name + '")}'
            });
            $.ajax({
                url: "http://localhost:9000/graphql",
                contentType: "application/json",
                type: 'POST',
                data: query,
                success: function(result) {
                    $("#SayhelloDiv").html("<h5 class='text-success'>" + result.data.sayHello + "</h5>");
                    $(".spinner").addClass("d-none");
                },
                error(xhr, options, msg) {
                    var msg = typeof xhr.responseJSON != "undefined" ? xhr.responseJSON.errors[0].message : xhr.statusText;
                    $(".errorModal .modal-body p").html(msg);
                    $('.errorModal').modal('show');
                    $(".spinner").addClass("d-none");
                }
            });
        });

        $("#btnStudents").off('click').on('click', function() {
            $(".spinner").removeClass("d-none");
            let query = JSON.stringify({
                query: '{students{id,fullName,college {id,name,location,rating}}}'
            });
            $.ajax({
                url: "http://localhost:9000/graphql",
                contentType: "application/json",
                type: 'POST',
                data: query,
                success: function(result) {
                    app.students = result.data.students;
                    app.vue_students.students = app.students;
                    $(".spinner").addClass("d-none");
                },
                error(xhr, options, msg) {
                    var msg = typeof xhr.responseJSON != "undefined" ? xhr.responseJSON.errors[0].message : xhr.statusText;
                    $(".errorModal .modal-body p").html(msg);
                    $('.errorModal').modal('show');
                    $(".spinner").addClass("d-none");
                }
            });
        });

        $("#btnMySQLStudents").off('click').on('click', function() {
            $(".spinner").removeClass("d-none");
            let query = JSON.stringify({
                query: '{studentsmy{id,fullName,college {id,name,location,rating}}}'
            });
            $.ajax({
                url: "http://localhost:9000/graphql",
                contentType: "application/json",
                type: 'POST',
                data: query,
                success: function(result) {
                    app.students = result.data.studentsmy;
                    app.vue_students.students = app.students;
                    $(".spinner").addClass("d-none");
                },
                error(xhr, options, msg) {
                    var msg = typeof xhr.responseJSON != "undefined" ? xhr.responseJSON.errors[0].message : xhr.statusText;
                    $(".errorModal .modal-body p").html(msg);
                    $('.errorModal').modal('show');
                    $(".spinner").addClass("d-none");
                }
            });
        });

        $("#btnStudentByID").off('click').on('click', function() {
            if ($("#txtStudenId").val() === '') {
                $(".errorModal .modal-body p").html("No dejes el campo id vacio");
                $('.errorModal').modal('show');
                return;
            }
            $(".spinner").removeClass("d-none");
            let tmp_id = $("#txtStudenId").val();
            let query = JSON.stringify({
                query: '{studentById(id:"' + tmp_id + '"){id,fullName,college {id,name,location,rating}}}'
            });
            $.ajax({
                url: "http://localhost:9000/graphql",
                contentType: "application/json",
                type: 'POST',
                data: query,
                success: function(result) {
                    if (result.data.studentById === null) {
                        $(".errorModal .modal-body p").html("No hay registros con el ID: " + tmp_id);
                        $('.errorModal').modal('show');
                    } else {
                        app.students = [result.data.studentById];
                        app.vue_students.students = app.students;
                    }
                    $(".spinner").addClass("d-none");
                    $("#txtStudenId").val("");
                },
                error(xhr, options, msg) {
                    var msg = typeof xhr.responseJSON != "undefined" ? xhr.responseJSON.errors[0].message : xhr.statusText;
                    $(".errorModal .modal-body p").html(msg);
                    $('.errorModal').modal('show');
                    $(".spinner").addClass("d-none");
                }
            });
        });

        $("#btnAgGridStudents").off('click').on('click', function() {
            $(".spinner").removeClass("d-none");
            let query = JSON.stringify({
                query: '{studentsag{id,nombre,colegio,locacion}}'
            });
            $.ajax({
                url: "http://localhost:9000/graphql",
                contentType: "application/json",
                type: 'POST',
                data: query,
                success: function(result) {
                    app.studentsag = result.data.studentsag;
                    app.ag.gridOptions.api.setRowData(app.studentsag);
                    $(".spinner").addClass("d-none");
                },
                error(xhr, options, msg) {
                    var msg = typeof xhr.responseJSON != "undefined" ? xhr.responseJSON.errors[0].message : xhr.statusText;
                    $(".errorModal .modal-body p").html(msg);
                    $('.errorModal').modal('show');
                    $(".spinner").addClass("d-none");
                }
            });
        });

        $("#btnAgGridMySQLStudents").off('click').on('click', function() {
            $(".spinner").removeClass("d-none");
            let query = JSON.stringify({
                query: '{studentsmy{id,nombre,colegio,locacion}}'
            });
            $.ajax({
                url: "http://localhost:9000/graphql",
                contentType: "application/json",
                type: 'POST',
                data: query,
                success: function(result) {
                    app.studentsag = result.data.studentsmy;
                    app.ag.gridOptions.api.setRowData(app.studentsag);
                    $(".spinner").addClass("d-none");
                },
                error(xhr, options, msg) {
                    var msg = typeof xhr.responseJSON != "undefined" ? xhr.responseJSON.errors[0].message : xhr.statusText;
                    $(".errorModal .modal-body p").html(msg);
                    $('.errorModal').modal('show');
                    $(".spinner").addClass("d-none");
                }
            });
        });

        // Plugins
        $('.errorModal').modal({
            "backdrop": "static",
            "show": false
        });

        $('[data-toggle="tooltip"]').tooltip();
    };

    const _vue = function() {
        app.vue_students = new Vue({
            el: '.student_tb',
            data: {
                students: app.students
            }
        });
    };

    const _agGrid = function() {
        // specify the columns
        const columnDefs = [{
            field: "id",
            sortable: true,
            filter: true
        }, {
            field: "nombre",
            sortable: true,
            filter: true
        }, {
            field: "colegio",
            sortable: true,
            filter: true
        }, {
            field: "locacion",
            sortable: true,
            filter: true
        }];

        // specify the data
        /*const rowData = [{
            id: "1",
            nombre: "Celica Foo",
            colegio: "Chaf",
            locacion: "ugly place"
        }, {
            id: "2",
            nombre: "Toyota yu",
            colegio: "Fanc",
            locacion: "fancy location"
        }, {
            id: "3",
            nombre: "Bret brat",
            colegio: "Chaf",
            locacion: "ugly place"
        }];*/

        // let the grid know which columns and what data to use
        const gridOptions = {
            columnDefs: columnDefs,
            rowData: []
        };

        // lookup the container we want the Grid to use
        const eGridDiv = document.querySelector('#myGrid');

        // create the grid passing in the div to use together with the columns & data we want to use
        app.ag = new agGrid.Grid(eGridDiv, gridOptions);
    };

    root.students = [];
    root.studentsag = [];
    root.init = _init;

    return root;
})();

$(document).ready(function() {
    window.app.init();
});