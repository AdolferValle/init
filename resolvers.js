module.exports = (async function(){
    const db = await require("./db");
    const Query = {
        test: () => 'Prueba exitosa, servidor GraphQL esta activo y funcionando !!',
        greeting: () => 'Saludos GraphQL desde tu nuevo equipo !!',
        sayHello: (root, args, context, info) => 'Hola ' + args.name + ', servidor GraphQL te saluda !!',
        students: () => db.students.list(),
        studentById: (root, args, context, info) => {
            return db.students.get(args.id);
        },
        studentsag: () => db.studentsag.list(),
        studentagById: (root, args, context, info) => {
            return db.studentsag.get(args.id);
        },
        studentsmy: () => db.studentsmy,
        studentmyById: (root, args, context, info) => {
            return db.studentsmy.filter(d => d.id == args.id);
        }
    }
    const Student = {
        fullName: (root, args, context, info) => {
            return root.firstName + " " + root.lastName
        },
        college: (root) => {
            return db.colleges.get(root.collegeId);
        }
    }
    const StudentAG = {
        nombre: (root, args, context, info) => {
            return root.firstName + " " + root.lastName
        },
        colegio: (root) => {
            return db.colleges.get(root.collegeId).name;
        },
        locacion: (root) => {
            return db.colleges.get(root.collegeId).location;
        }
    }

    const StudentMY = {
        nombre: (root, args, context, info) => {
            return root.name + " " + root.lastname
        },
        colegio: (root) => {
            return db.collegesmy.filter(d => d.id == root.collegeId)[0].name;
        },
        locacion: (root) => {
            return db.collegesmy.filter(d => d.id == root.collegeId)[0].location;
        }
    }

    return {
        Query,
        Student,
        StudentAG,
        StudentMY
    };
})();