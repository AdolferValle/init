(async function(){
	const fs = require('fs');
	const db = require('./db');
	const path = require('path');
	const express = require('express');
	const serveIndex = require('serve-index');
	const resolvers = await require('./resolvers');
	const {
		ApolloServer
	} = require('apollo-server-express');

	const typeDefs = fs.readFileSync('./schema.graphql', {
		encoding: 'utf-8'
	});
	const graphQLserver = new ApolloServer({
		typeDefs,
		resolvers
	});

	const app = express();
	const port = process.env.PORT || 9000;
	const host = "127.0.0.1";

	const userIsAllowed = function(callback) {
		// this function would contain your logic, presumably asynchronous,
		// about whether or not the user is allowed to see files in the
		// protected directory; here, we'll use a default value of "false"
		callback(false);
	};

	//graphQL server
	graphQLserver.applyMiddleware({ app });
	// webserver
	app.use('/', express.static(__dirname + '/public'));
	app.use('*', serveIndex(__dirname + '/views'));

	app.set('view engine', 'ejs');
	app.set('views', path.join(__dirname, 'public'));

	// forbidden routes
	app.get('/views/*', function(req, res, next) {
		userIsAllowed(function(allowed) {
			if (allowed) {
				next(); // call the next handler, which in this case is express.static
			} else {
				res.redirect('/');
			}
		});
	});
	app.get('/assets/*', function(req, res, next) {
		userIsAllowed(function(allowed) {
			if (allowed) {
				next(); // call the next handler, which in this case is express.static
			} else {
				res.redirect('/');
			}
		});
	});
	// entry point
	app.get('*', function(req, res) {
		if (req.url == "/") req.url = "/home";

		let tmp_config = {
			req: req.url,
			error: "",
			view: req.url.replace("/", "")
		};

		if (req.url != "/") {
			fs.access("./public/views" + req.url + ".html", fs.F_OK, function(err) {
				if (err) {
					tmp_config.error = "404 - " + req.url + " no such file or directory! ";
					tmp_config.view = "error";
				}
				res.render('index', tmp_config);
			});
		}
	});
	// start server
	app.listen(port, host, function() {
		console.log('Server running at port ' + port + ': http://' + host + ':' + port);
	});
})();